﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SablonaNakup
{

    public struct Smer
    {

        private sbyte x, y;

        public Smer(sbyte x, sbyte y)
        {
            this.x = (sbyte)Math.Sign(x);
            this.y = (sbyte)Math.Sign(y);
        }

        public sbyte X {
            get
            {
                return x;
            }
            set
            {
                x = (sbyte)Math.Sign(value);
            }
        }
        public sbyte Y
        {
            get
            {
                return y;
            }
            set
            {
                y = (sbyte)Math.Sign(value);
            }
        }
    }

    abstract class Osoba
    {
        /// <summary>
        /// Generátor náhodných čísel, který můžete používat.
        /// Nepoužívejte vlastní vytvořený generátor (new Random). Vysvětlení buď vygooglete nebo se na něj zeptejte.
        /// </summary>
        protected Random nahoda;

        public Random Nahoda { set => nahoda = value; }

    }

    abstract class Nakupujici : Osoba
    {
        /// <summary>
        /// Krok nakupujiciho.
        /// </summary>
        /// <param name="castMapy">Vyrez mapy 2x2 okolo nakupujiciho. Neviditelna pole jsou oznacena znakem ?.</param>
        /// <param name="penize">Pocet penez, ktere nakupujici ma prave v penezence.</param>
        /// <param name="mojeSouradnice">Soucasne souradnice nakupujiciho vzhledem k levemu hornimu rohu.</param>
        /// <returns>Smer, kterym se nakupujici ma vydat.</returns>
        abstract public Smer Krok(char[,] castMapy, int penize, Souradnice mojeSouradnice);
    }

    abstract class Zlodej : Osoba
    {
        /// <summary>
        /// Krok zlodeje
        /// </summary>
        /// <param name="castMapy">Cela mapa se skrytyma nakupujicima, ktere jsem nedavno okradl.</param>
        /// <param name="penize">Pocet penez, ktere zlodej ma.</param>
        /// <param name="mojeSouradnice">Souradnice zlodeje v mape.</param>
        /// <param name="opakovani">Po kolikate byla tato funkce zavolana. Zacina cislem 1. Pokud se zlodej rozhodne jit neplatnym smerem, bude funkce zavolana
        /// znovu s timto parametrem o jedna vyssim nez posledne. K opakovani dojde maximalne devetkrat.</param>
        /// <returns>Smer, kterym se zlodej ma vydat.</returns>
        abstract public Smer Krok(char[,] castMapy, int penize, Souradnice mojeSouradnice, int opakovani);
    }
}
